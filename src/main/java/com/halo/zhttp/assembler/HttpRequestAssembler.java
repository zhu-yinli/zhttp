package com.halo.zhttp.assembler;

import org.apache.http.client.methods.HttpRequestBase;

public interface HttpRequestAssembler {

    void assemble(HttpRequestBase httpRequest) throws AssemblerException;
}
