package com.halo.zhttp.assembler;

import com.halo.zhttp.assembler.builder.AuthBuilder;
import com.halo.zhttp.assembler.builder.BuilderException;
import org.apache.http.client.methods.HttpRequestBase;

public class AuthHeaderAssembler implements HttpRequestAssembler {
    private AuthBuilder authBuilder;

    public AuthHeaderAssembler(AuthBuilder authBuilder) {
        this.authBuilder = authBuilder;
    }

    @Override
    public void assemble(HttpRequestBase httpRequest) throws AssemblerException {
        try {
            authBuilder.build(httpRequest);
        } catch (BuilderException e) {
            throw new AssemblerException("Assemble http request error.", e);
        }
    }
}
