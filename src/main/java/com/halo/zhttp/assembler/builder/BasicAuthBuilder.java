package com.halo.zhttp.assembler.builder;

import com.halo.zhttp.utils.CommonUtils;
import com.halo.zhttp.utils.UtilsException;
import org.apache.http.client.methods.HttpRequestBase;

public class BasicAuthBuilder implements AuthBuilder {
    private String user;

    private String password;

    public BasicAuthBuilder(String user, String password){
        this.user = user;
        this.password = password;
    }

    @Override
    public void build(HttpRequestBase httpRequest) throws BuilderException {
        String authorizationString;
        try {
            authorizationString = CommonUtils.getAuthorizationString(user, password);
        } catch (UtilsException e) {
            throw new BuilderException("Build authorization header error. ", e);
        }
        httpRequest.addHeader("Authorization", "Basic " + authorizationString);
    }
}
