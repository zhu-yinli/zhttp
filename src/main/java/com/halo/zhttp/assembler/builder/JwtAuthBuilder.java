package com.halo.zhttp.assembler.builder;

import com.halo.zhttp.utils.CommonUtils;
import org.apache.http.client.methods.HttpRequestBase;

public class JwtAuthBuilder implements AuthBuilder {
    private String user;

    private String password;

    public JwtAuthBuilder(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public void build(HttpRequestBase httpRequest) throws BuilderException {
        String authorizationString = CommonUtils.getJwtAuthorizationString(user, password);

        httpRequest.addHeader("Authorization", "Bearer " + authorizationString);
    }
}
