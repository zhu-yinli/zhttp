package com.halo.zhttp.assembler.builder;

import org.apache.http.client.methods.HttpRequestBase;

public interface AuthBuilder {
    void build(HttpRequestBase httpRequest) throws BuilderException;
}
